
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php
    echo empty($title) ? "XE" : $title;
   ?></title>
  <script src="<?php echo base_url('assets/js/jquery-3.4.1.min.js');?>"></script>
  <script src="<?php echo base_url('assets/js/hr_managment.js');?>"></script>
  <script src="<?php echo base_url('assets/js/permissions.js');?>"></script>
  <script src="<?php echo base_url('assets/js/delete_rows.js');?>"></script>
  <script src="<?php echo base_url('assets/js/marketing.js');?>"></script>
  <script src="<?php echo base_url('assets/js/product.js');?>"></script>
  <script src="<?php echo base_url('assets/js/meterial_delivery.js');?>"></script>
  <script src="<?php echo base_url('assets/js/meterial_usage.js');?>"></script>
  <script src="<?php echo base_url('assets/js/account_book.js');?>"></script>
  <script src="<?php echo base_url('assets/js/delivery_challan.js');?>"></script>
  <script src="<?php echo base_url('assets/js/project.js');?>"></script>
  <script src="<?php echo base_url('assets/js/sales.js');?>"></script>
  <script src="<?php echo base_url('assets/js/purchase.js');?>"></script>
  <script src="<?php echo base_url('assets/js/setting.js');?>"></script>
  <script src="<?php echo base_url('assets/js/customers.js');?>"></script>

  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/ti-icons/css/themify-icons.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/css/vendor.bundle.base.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/select2/select2.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/vendors/select2-bootstrap-theme/select2-bootstrap.min.css');?>">  

  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-toast-plugin/jquery.toast.min.css');?>">
  <!-- endinject -->

   <link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-bar-rating/css-stars.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/vendors/mdi/css/materialdesignicons.min.css');?>">



  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/summernote/dist/summernote-bs4.css');?>">
   <link rel="stylesheet" href="<?php echo base_url('assets/vendors/quill/quill.snow.css');?>">
   <link rel="stylesheet" href="<?php echo base_url('assets/vendors/simplemde/simplemde.min.css');?>">

  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/x-editable/bootstrap-editable.css');?>">

  <!-- Required meta tags -->
  <!-- endinject -->
    

  <link rel="stylesheet" href="<?php echo base_url('assets/css/client.css');?>">  
      
  <!-- Plugin css for this page -->
   <!-- Plugin css for this page -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css');?>">
  <!-- End plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css');?>">
  <!-- endinject -->
<link rel="stylesheet" href="<?php echo base_url('assets/vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.min.css');?>">

<link rel="stylesheet" href="<?php echo base_url('assets/vendors/dropify/dropify.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-file-upload/uploadfile.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-tags-input/jquery.tagsinput.min.css');?>">

<link rel="stylesheet" href="<?php echo base_url('assets/vendors/dropzone/dropzone.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/font-awesome/css/font-awesome.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-bar-rating/bars-1to10.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-bar-rating/bars-horizontal.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-bar-rating/bars-movie.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-bar-rating/bars-pill.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-bar-rating/bars-reversed.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-bar-rating/bars-square.css');?>">  
<link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-bar-rating/bootstrap-stars.css');?>">  
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-bar-rating/examples.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-bar-rating/fontawesome-stars-o.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-bar-rating/fontawesome-stars.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-asColorPicker/css/asColorPicker.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/vertical-layout-light/style.css');?>">


</head>

<body class="sidebar-dark">
  <input type="hidden" id="base_url" value="<?php echo base_url('');?>">
  <div class="container-scroller">
  <div class="container-fluid page-body-wrapper">
<!-- ############################################################## -->
         <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="display-2">Estimate&nbsp;#<?php echo $invoice['no']; ?> </h4>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Customer</label>
                          <div class="col-sm-12">
                               <b><?php echo $invoice['full_name']; ?></b>,&nbsp;
                               <font size="2">
                                <?php echo $invoice['city'].",&nbsp;".$invoice['mobile1']; ?>    </font>
                          </div>
                        </div>
                      </div>
                    </div>
           <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Cart List</h4>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr class="table-dark">
                          <th>No</th>
                          <th>Item</th>
                          <th>Qty</th>
                          <th>Price</th>
                          <th>Discount</th>
                          <th>GST</th>
                          <th>Total Price</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $slno = 0;
                        $subtotal = 0.0;
                        $discound = 0.0;
                        $gst = 0.0;
                        foreach ($bill as $row) {
                          $slno =+ 1;
                          $subtotal += $row['total']; 
                          $discound += $row['discound'];
                          $gst += $row['gst'];
                         ?>
                        <tr>
                          <td><?php echo $slno; ?></td>
                          <td><?php echo $row['item']; ?></td>
                          <td><?php echo $row['quantity']."  ".$row['unit']; ?></td>
                          <td>₹<?php echo $row['price']; ?></td>
                          <td>₹<?php echo $row['discound']; ?></td>
                          <td>₹<?php echo $row['gst']; ?></td>
                          <td><b>₹<?php echo $row['total']; ?></b></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

                   
                    

                   <div class="row">
                      <div class="col-md-12">
                        <div class="form-group row">

                            <div class="col-sm-6">
                             Price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;₹<?php echo $subtotal + $discound - $gst; ?><br>
                             Discount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;₹<?php echo $discound; ?><br>
                             <?php if ($gst > 0) { ?>Taxable Value: ₹<?php echo $subtotal - $gst; ?> <?php } ?>
                              <br>
                            <?php if ($invoice["sale_type"] == 0 && $gst > 0){?>
                              CGST:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;₹<?php echo $gst/2;?><br>
                              SGST:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;₹<?php echo $gst/2;?>
                            
                            <?php }else{?>

                              IGST:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;₹<?php echo $gst;?>
                                       
                            <?php }   ?>  

                             <br><br>
                             CESS<br>
          <?php 
            $total_cess = 0;                     
          if (sizeof($cess) > 0)
          {
          foreach($cess as $row)
          { 
            if ($row['cess'] > 0)
            {
            $cess_rate =($subtotal + $discound - $gst)*$row['cess']/100;
            $total_cess += $cess_rate;
            ?> <div style="margin-left: 17%;margin-top: -10px;padding: 6px;">:₹<?php echo $cess_rate."&nbsp;(".$row['name']; ?>)</div>
        <?php 
            }
          }
            }

          ?>
                             <div class="display-4 text-lg-left text-sm-center"><font size="5" color="#0082DC">Total Amount: ₹<?php echo $subtotal + $total_cess;?></font></div>
                            </div>

                          <div class="col-sm-6 text-center">
                           <div class="form-group pull-right row">
                            <label class="col-sm-12 col-form-label">About Sale</label>
                             <p class="card-description "><?php echo $invoice['about']; ?> </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                    <div class="row">
                      <div class="col-md-6 text-center">
                        <div class="form-group row">
                          <label class="col-sm-12 col-form-label"><font size="5" class="text-dark">Cash Recieved </font><font size="5" class="text-success">₹<?php echo $invoice['cash_recieved']; ?></font> by <?php echo $invoice['mode']; ?><br>
                        </div>
                      </div>

                      <div class="col-md-6 text-center">
                        <div class="form-group row pull-right">
                          <label class="col-sm-12 col-form-label">
                          <b>Sold on:</b>&nbsp;<?php echo $row['created_at'];?><br>
                          <b>Soled by:</b>&nbsp;<?php echo $invoice['emp_name']; ?></label>
                        </div>
                      </div>
                    </div>
                    
                </div>

              </div>
            </div>
          </div>
        <!-- content-wrapper ends -->

<!------#######################################---->
        <!-- partial:../../partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Developed by <a href="https://www.xeobrain.com/" target="_blank">Xeobrain</a> in india </span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Ver. 1.0.1 </span>
          </div>
        </footer>
        <!-- partial -->
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <script src="<?php echo base_url('assets/vendors/js/vendor.bundle.base.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/typeahead.js/typeahead.bundle.min.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/select2/select2.min.js');?>"></script>



  <!-- plugins:js -->
  <script src="<?php echo base_url('assets/vendors/jquery-bar-rating/jquery.barrating.min.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/jquery-asColor/jquery-asColor.min.js');?>"></script>
 
  <script src="<?php echo base_url('assets/vendors/moment/moment.min.js');?>"></script>

        <!-- plugin js for this page -->
  <script src="<?php echo base_url('assets/vendors/summernote/dist/summernote-bs4.min.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/tinymce/tinymce.min.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/quill/quill.min.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/simplemde/simplemde.min.js');?>"></script> 
 
  
  <script src="<?php echo base_url('assets/vendors/jquery-toast-plugin/jquery.toast.min.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/jquery-asGradient/jquery-asGradient.min.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/jquery-asColorPicker/jquery-asColorPicker.min.js');?>"></script>
 
 
 <script src="<?php echo base_url('assets/vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/x-editable/bootstrap-editable.min.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js');?>"></script> 



  <script src="<?php echo base_url('assets/vendors/dropify/dropify.min.js');?>"></script>
  <script src=".<?php echo base_url('assets/vendors/jquery-file-upload/jquery.uploadfile.min.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/jquery-tags-input/jquery.tagsinput.min.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/dropzone/dropzone.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/jquery.repeater/jquery.repeater.min.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/inputmask/jquery.inputmask.bundle.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/moment.min.js');?>"></script>

    <script src="<?php echo base_url('assets/vendors/sweetalert/sweetalert.min.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/jquery.avgrund/jquery.avgrund.min.js');?>"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
   <!-- Plugin js for this page-->
  <script src="<?php echo base_url('assets/vendors/datatables.net/jquery.dataTables.js');?>"></script>
  <script src="<?php echo base_url('assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js');?>"></script>
  <!-- End plugin js for this page-->
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="<?php echo base_url('assets/js/off-canvas.js');?>"></script>
  <script src="<?php echo base_url('assets/js/hoverable-collapse.js');?>"></script>
  <script src="<?php echo base_url('assets/js/template.js');?>"></script>
  <script src="<?php echo base_url('assets/js/settings.js');?>"></script>
  <script src="<?php echo base_url('assets/js/todolist.js');?>"></script>


  <!-- endinject -->
  <!-- Custom js for this page-->
    <!-- Custom js for this page-->
  <script src="<?php echo base_url('assets/js/data-table.js');?>"></script>
  <!-- End custom js for this page-->
    <!-- Custom js for this page-->
  <script src="<?php echo base_url('assets/js/alerts.js');?>"></script>
  <script src="<?php echo base_url('assets/js/avgrund.js');?>"></script>


  <!-- End custom js for this page-->
  <!-- End custom js for this page-->
    <!-- Custom js for this page-->
  <script src="<?php echo base_url('assets/js/formpickers.js');?>"></script>
  <script src="<?php echo base_url('assets/js/form-addons.js');?>"></script>
  <script src="<?php echo base_url('assets/js/x-editable.js');?>"></script>
  <script src="<?php echo base_url('assets/js/dropify.js');?>"></script>
  <script src="<?php echo base_url('assets/js/dropzone.js');?>"></script>
  <script src="<?php echo base_url('assets/js/jquery-file-upload.js');?>"></script>
  <script src="<?php echo base_url('assets/js/form-repeater.js');?>"></script>
  <script src="<?php echo base_url('assets/js/inputmask.js');?>"></script>

  <!-- End custom js for this page-->
    <script src="<?php echo base_url('assets/js/file-upload.js');?>"></script>
  <script src="<?php echo base_url('assets/js/typeahead.js');?>"></script>
  <script src="<?php echo base_url('assets/js/select2.js');?>"></script>
  <script src="<?php echo base_url('assets/js/profile-demo.js');?>"></script>


  
  <script src="<?php echo base_url('assets/js/toastDemo.js');?>"></script>
  <script src="<?php echo base_url('assets/js/editorDemo.js');?>"></script>
 <!--- <script src="<?php //echo base_url('assets/js/desktop-notification.js');?>"></script>-->
 <script type="text/javascript">
   
   $('.datepicker-days').show()
 </script>
</body>

</html>